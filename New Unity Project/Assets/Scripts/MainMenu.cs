﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public AudioSource source;
	public AudioClip hover;
	public AudioClip click;

	public void PlayGame (){
		source.PlayOneShot (click);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void QuitGame (){
		Application.Quit ();
	}

	public void onHover() {
		source.PlayOneShot (hover);
	}

	public void onClick() {
		source.PlayOneShot (click);
	}

}
