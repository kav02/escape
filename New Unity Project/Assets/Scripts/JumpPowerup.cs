﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPowerup : MonoBehaviour {

    public GameObject Player;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Player")
        {
            Destroy(this.gameObject);
            StartCoroutine(TimeBetween());
        }
    }

    IEnumerator TimeBetween()
    {
        Player.transform.GetComponent<PlayerMovement>().jumpForce = 600;
        yield return new WaitForSeconds(3);
        Debug.Log("12 seconds over");
        Player.transform.GetComponent<PlayerMovement>().jumpForce = 400;
    }
}
