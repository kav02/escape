﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

 

    public AudioSource source;
    public AudioClip jumpnoise;

	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	public float maxContactDistance = 0.1f;

	private Rigidbody2D rigidBody;
	private bool grounded = false;
	private bool jump = false;
	private float horizontal = 0.0f;

	int groundMaskId;
	// Use this for initialization
	void Start () {

		rigidBody = GetComponent<Rigidbody2D> ();
		groundMaskId = LayerMask.GetMask ("Ground");
	}

	void FixedUpdate() {
		if (Mathf.Abs(horizontal * rigidBody.velocity.x) < maxSpeed) {
			rigidBody.AddForce (Vector2.right * horizontal * moveForce);
            if (horizontal < 0)
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
            else if (horizontal > 0)
            {
                this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
		}
		capVelocity();

		if (jump == true) {
			rigidBody.AddForce (new Vector2 (0f, jumpForce));
			jump = false;
		}


	}

	void capVelocity(){
		if (Mathf.Abs (rigidBody.velocity.x) > maxSpeed) {
			float signedMaxSpeed = Mathf.Sign (rigidBody.velocity.x) * maxSpeed;
			rigidBody.velocity = new Vector2 (signedMaxSpeed, rigidBody.velocity.y);

		}
	}
	
	// Update is called once per frame
	void Update () {
		checkGrounded ();
		horizontal = Input.GetAxis ("Horizontal");
		if (Input.GetButtonDown ("Jump") && grounded) {
			Debug.Log ("Conditions for jump are met");
			jump = true;
            source.PlayOneShot(jumpnoise);
		}
	}

	void checkGrounded() {
		BoxCollider2D bc = this.GetComponent<BoxCollider2D> ();
		Vector2 spriteBottom = transform.position;
		float spriteHeight = bc.size.y;
		spriteBottom.y -= spriteHeight;

		RaycastHit2D hit = Physics2D.Raycast (spriteBottom, Vector2.down, groundMaskId);
		if (hit.distance < maxContactDistance) {
			grounded = true;
		} else {
			grounded = false;
		}
	}
}
