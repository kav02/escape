﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerDeathOnHit : MonoBehaviour {
    public Transform parent;
    public GameObject menu;
    public Text Text;
    public AudioSource source;
    public AudioClip die;
    public AudioClip ticking;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            source.PlayOneShot(die);
            Debug.Log(collision.transform.GetChild(0).name);
            collision.gameObject.transform.GetChild(0).transform.SetParent(parent);
            menu.SetActive(true);
            Destroy(collision.gameObject);
            StartCoroutine(GameOver());
        }
    }

    IEnumerator GameOver()
    {
        for (int i = 5; i != 0; i--)
        {
            source.PlayOneShot(ticking);
            Text.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
