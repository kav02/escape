﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	public Light PlayerLight;
	public float Strength = 0f;

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.name == "Player") {
			print ("PowerUp!");
			PlayerLight.range = PlayerLight.range + Strength;
			Destroy (this.gameObject);
		}
	}
}
